//----------------------------------------------------------------
// Name        : main.cpp
// Author      : Remco Hoogenboezem
// Version     :
// Copyright   :
// Description : 
//----------------------------------------------------------------
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <getopt.h>
#include <string.h>
//----------------------------------------------------------------
using namespace std;
//----------------------------------------------------------------
#define TABLE_FILE      't'
#define ANNOVAR_FILES   'a'
#define ANNOVAR_NAMES   'n'
#define HELP            'h'
#define SHORT_OPTIONS   "t:a:n:h"
//----------------------------------------------------------------
struct option longOptions[] =
{
    {"table-file",required_argument,NULL,TABLE_FILE},
    {"annovar-files",required_argument,NULL,ANNOVAR_FILES},
    {"annovar-names",required_argument,NULL,ANNOVAR_NAMES},
    {"help",no_argument,NULL,HELP},
    {0, 0, 0, 0}
};
//----------------------------------------------------------------
vector<string> & tokenize(const string & sString,const char * pDelimiter)
{
    int                     iStrLen;
    char *                  pString;
    static vector<string>   vTokens;
    
    vTokens.clear();
    
    if((iStrLen=sString.size())>0)
    {
        pString=(char*)memcpy(new char[iStrLen+1],sString.c_str(),iStrLen+1);

        while(pString)
        {
            vTokens.push_back(string(strsep(&pString,pDelimiter)));
        }
    
        delete [] pString;
    }
    
    return vTokens;
}
//----------------------------------------------------------------
int main(int argc, char** argv)
{
    bool                    bShowHelp;
    
    int                     iOption;
    int                     iOptionIndex;
    int                     nFiles;
    int                     iFile;
    
    
    string                  sTableFileName;
    string                  sLine;
    string                  sKey;
    
    vector<string>          vAnnovarFileNames;
    vector<string>          vAnnovarNames;
    vector<string>          vLine;
    vector<set<string> >    vAnnovarSets;    
    
    ifstream                annovarFile;
    ifstream                tableFile;
    
    //----------------------------------------------------------------
    //Get input arguments
    //----------------------------------------------------------------
    
    bShowHelp=(argc==1);
    
    while((iOption=getopt_long(argc,argv,SHORT_OPTIONS,longOptions,&iOptionIndex))>=0)
    {
        switch(iOption)
        {
            case TABLE_FILE:
                
                sTableFileName=string(optarg);
                break;
                
            case ANNOVAR_FILES:
                
                vAnnovarFileNames=tokenize(string(optarg),",");
                break;

            case ANNOVAR_NAMES:
                
                vAnnovarNames=tokenize(string(optarg),",");
                break;
                
            case HELP:
            default:
                
                bShowHelp=true;
                break;
        }
    }
    
    //----------------------------------------------------------------
    //Show help
    //----------------------------------------------------------------
   
    if(bShowHelp)
    {
        cerr << "assign_annovar_files [options] > new_table_file.txt"                                                 << endl;
        cerr                                                                                                            << endl;
        cerr << "--table-file <text>        Single table file from tableannovar script (required)"                      << endl;
        cerr << "--annovar-files <text>     One or more annovar files from convert2annovar script (required)"           << endl;
        cerr << "--annovar-names <text>     One or more annovar names corresponding to the annovar-files (required)"    << endl;
        cerr                                                                                                            << endl;
        
        return 0;
    }

    //----------------------------------------------------------------
    //Check input arguments
    //----------------------------------------------------------------
    
    if(sTableFileName.empty())
    {
        cerr << "Error: Please specify one table file (--help)" << endl;
        return 1;
    }
    
    if(vAnnovarFileNames.empty())
    {
        cerr << "Error: Please specify one or more comma separated annovar files (--help)" << endl;
        return 1;
    }
    
    if(vAnnovarFileNames.size()!=vAnnovarNames.size())
    {
        cerr << "Error: Number and order of annovar files and annovar names must agree (--help)" << endl;
        return 1;
    }
    
    //----------------------------------------------------------------
    //Read the annovar files
    //----------------------------------------------------------------
    
    nFiles=vAnnovarFileNames.size();
    vAnnovarSets.assign(nFiles,set<string>());
    
    for(iFile=0;iFile<nFiles;iFile++)
    {
        annovarFile.open(vAnnovarFileNames[iFile].c_str(),ifstream::in);
        
        if(annovarFile.good()==false)
        {
            cerr << "Error: Could not open annovar file: " << vAnnovarFileNames[iFile] << endl;
            return 1;
        }
        
        for(getline(annovarFile,sLine);annovarFile.good();getline(annovarFile,sLine))
        {
            if(sLine.empty())
            {
                continue;
            }

            vLine=tokenize(sLine,"\t");
            
            if(vLine.size()<5)
            {
                cerr << "Error: Insufficient number of columns in annovar file: " << vAnnovarFileNames[iFile] << endl;
                return 1;
            }
            
            vAnnovarSets[iFile].insert(vLine[0]+string("\t")+vLine[1]+string("\t")+vLine[2]+string("\t")+vLine[3]+string("\t")+vLine[4]);
        }
        
        if(annovarFile.eof()==false)
        {
            cerr << "Error: Could not read annovar file: " << vAnnovarFileNames[iFile] << endl;
            return 1;
        }
        
        annovarFile.close();
    }
    
    //----------------------------------------------------------------
    //Read the table file
    //----------------------------------------------------------------
    
    tableFile.open(sTableFileName.c_str(),ifstream::in);
    
    if(tableFile.good()==false)
    {
        cerr << "Error: Could not open table file" << endl;
        return 1;
    }
    
    getline(tableFile,sLine);
    
    if(tableFile.good()==false)
    {
        cerr << "Error: Could not read table file" << endl;
        return 1;
    }
    
    cout << sLine;
    
    for(iFile=0;iFile<nFiles;iFile++)
    {
        cout << '\t' << vAnnovarNames[iFile];
    }
    
    cout << '\n';
    
    for(getline(tableFile,sLine);tableFile.good();getline(tableFile,sLine))
    {
        if(sLine.empty())
        {
            continue;
        }
        
        vLine=tokenize(sLine,"\t");
        
        if(vLine.size()<5)
        {
            cerr << "Error: Insufficient number of columns in table file" << endl;
            return 1;
        }
        
        cout << sLine;

        sKey=vLine[0]+string("\t")+vLine[1]+string("\t")+vLine[2]+string("\t")+vLine[3]+string("\t")+vLine[4];
        
        for(iFile=0;iFile<nFiles;iFile++)
        {
            cout << '\t' << vAnnovarSets[iFile].count(sKey);
        }
        
        cout << '\n';
    }
    
    
    //----------------------------------------------------------------
    //Done
    //----------------------------------------------------------------
    
    return 0;
}
//----------------------------------------------------------------
