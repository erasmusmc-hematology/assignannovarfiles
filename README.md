# AssignAnnovarFiles

Program that takes a combined list of variants from multiple variant callers and identifies in which other Annovar-annotated tables they are present. This code has only been texted on Linux machines, but it may work on Mac OS or on the Linux compatibility layer of Windows 10.

## Installation instructions

Clone the repository with:

`git clone https://gitlab.com/erasmusmc-hematology/assignannovarfiles`

Then enter the newly created directory and compile the program with the following command:

`make all`

You will find the newly created binary here:

`dist/Debug/GNU-Linux/assign_annovar_files`

## How to use it?

This is a command-line application that is used as follows:

`assign_annovar_files [options] > new_table_file.txt`

AssignAnnovarFiles takes the following arguments:

- --table-file: Single table file from tableannovar script (required)
- --annovar-files: One or more annovar files from convert2annovar script (required)
- --annovar-names: One or more annovar names corresponding to the annovar-files (required)

## Acknowledgements & Citation

This program was written by Remco Hoogenboezem at Erasmus MC. Please cite this repository if you use this code in your research.
